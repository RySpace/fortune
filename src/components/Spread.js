import React from 'react';
import Card from './Card';

const Spread = ({ cards }) => {

  const cardLayout = cards.map((card) => {
    return <Card key={card.id} card={card} />
  });

  // const descLayout = cards.map((card) => {
  //   return <Description key={card.id} card={card} />
  // });

  return(
    <div className="ui horizontal raised segments">{cardLayout}</div>
  );

};

export default Spread;
