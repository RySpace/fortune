import React from 'react';
import Spread from './Spread';
import Deck from './Deck';
import './App.css';

// var thing = new Deck();
// thing.shuffle();

class App extends React.Component {
  constructor() {
    super();

    const draw = this.drawCards();
    this.state = {
      cards: draw
    };
  }


  drawCards = () => {
    var deck = new Deck().shuffle();
    let drawArr = [];
    for(let i = 0; i <= 2; i++) {
      drawArr.push(deck['deck'][i]);
    }
    console.log("The draw array!");
    console.log(drawArr);
    return drawArr;
  }

  reDraw = () => {
    console.log("redraw");
    const draw = this.drawCards();
    this.setState({ cards: draw });
  }


  render(){
    return(
      <div>
        <div className="game-board ui container">
          <h1 className="ui segment title center aligned">Sierra Lenormand</h1>
          <Spread cards={this.state.cards} />
        </div>
        <div className="ui segment pentagram" onClick={this.reDraw}>

        </div>
      </div>
    );
  }
}

export default App;
