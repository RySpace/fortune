import React from 'react';
import './Card.css';

class Card extends React.Component {
  state = { turned: false }

  turnCard = () => {
    if(!this.state.turned) {
      this.setState({ turned: true });
    }
  }

  render(){
    if (this.state.turned) {
      return(
        <div className="ui segment card center aligned">
          <img src={this.props.card.front} alt={this.props.card.name}/>
          <div className="ui segment description">
            <h4 className="ui header">{this.props.card.name.toUpperCase()}</h4>
            <p>{this.props.card.keywords}</p>
          </div>
        </div>
      );
    }
    return(
      <div className="ui segment card center aligned" onClick={this.turnCard}>
        <img src={this.props.card.back} alt={this.props.card.name} />
      </div>
    );

  }

};

export default Card;
