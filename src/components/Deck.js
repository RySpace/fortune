import card_1 from '../images/card_1.png';
import card_2 from '../images/card_2.png';
import card_3 from '../images/card_3.png';
import card_4 from '../images/card_4.png';
import card_5 from '../images/card_5.png';
import card_6 from '../images/card_6.png';
import card_7 from '../images/card_7.png';
import card_8 from '../images/card_8.png';
import card_9 from '../images/card_9.png';
import card_10 from '../images/card_10.png';
import card_11 from '../images/card_11.png';
import card_12 from '../images/card_12.png';
import card_13 from '../images/card_13.png';
import card_14 from '../images/card_14.png';
import card_15 from '../images/card_15.png';
import card_16 from '../images/card_16.png';
import card_17 from '../images/card_17.png';
import card_18 from '../images/card_18.png';
import card_19 from '../images/card_19.png';
import card_20 from '../images/card_20.png';
import card_21 from '../images/card_21.png';
import card_22 from '../images/card_22.png';
import card_23 from '../images/card_23.png';
import card_24 from '../images/card_24.png';
import card_25 from '../images/card_25.png';
import card_26 from '../images/card_26.png';
import card_27 from '../images/card_27.png';
import card_28 from '../images/card_28.png';
import card_29 from '../images/card_29.png';
import card_30 from '../images/card_30.png';
import card_31 from '../images/card_31.png';
import card_32 from '../images/card_32.png';
import card_33 from '../images/card_33.png';
import card_34 from '../images/card_34.png';
import card_35 from '../images/card_35.png';
import card_36 from '../images/card_36.png';
import card_1_back from '../images/card_1_back.png';
import card_2_back from '../images/card_2_back.png';
import card_3_back from '../images/card_3_back.png';
import card_4_back from '../images/card_4_back.png';
import card_5_back from '../images/card_5_back.png';


class Deck {
  constructor() {
    this.deck = [
      {id: 1, name: 'rider', front: card_1, back: card_1_back, keywords: 'Energy, Passion, Speed, Activity, News, Messages'},
      {id: 2, name: 'clover', front: card_2, back: card_2_back, keywords: 'Luck, Lightheartedness, Small Happinesses, Opportunity, Being Untroubled, Comedy'},
      {id: 3, name: 'ship', front: card_3, back: card_3_back, keywords: 'Departure, Farewell, Distance, Voyage, Travel, Journey, Adventure'},
      {id: 4, name: 'house', front: card_4, back: card_4_back, keywords: 'Home, Establishment, Safety, Tradition, Custom, Privacy, Conservation'},
      {id: 5, name: 'tree', front: card_5, back: card_5_back, keywords: 'Growth, grounded, past connection, personal growth, spirituality, health'},
      {id: 6, name: 'clouds', front: card_6, back: card_1_back, keywords: 'Confusion, Unclarity, Misunderstanding, Insecurity, Doubt, Hidden Secrets'},
      {id: 7, name: 'snake', front: card_7, back: card_2_back, keywords: '	Desire, Seduction, Deception, Craving, Attraction, Sexuality, Wisdom, Forbidden Knowledge'},
      {id: 8, name: 'coffin', front: card_8, back: card_3_back, keywords: '	Ending, Dying, Funeral, Loss, Grief, Mourning, Sadness'},
      {id: 9, name: 'bouquet', front: card_9, back: card_4_back, keywords: 'Flattery, Social Life, Pleasantness, Cordiality, Etiquette, Politeness, Appreciation'},
      {id: 10, name: 'scythe', front: card_10, back: card_5_back, keywords: 'Accidents, Hasty Decisions, Danger, A Warning, Speed, Reckoning'},
      {id: 11, name: 'whip', front: card_11, back: card_1_back, keywords: 'Conflict, Discussions, Arguments, Debate, Scolding, Opposition, Objection'},
      {id: 12, name: 'birds', front: card_12, back: card_2_back, keywords: 'Worry, Excitement, Gossip, Chattering, Nervousness, Anxiety'},
      {id: 13, name: 'child', front: card_13, back: card_3_back, keywords: 'New Beginnings, Child, Toddler, Play , Inexperience, Innocence, Immaturity'},
      {id: 14, name: 'fox', front: card_14, back: card_4_back, keywords: 'Selfishness, Self Care, Trickery, Suspicion, Cunning, Caution'},
      {id: 15, name: 'bear', front: card_15, back: card_5_back, keywords: 'Power, Leadership, Dominance, Influence, Short temper, Strength of character, Boss'},
      {id: 16, name: 'stars', front: card_16, back: card_1_back, keywords: 'Hope, Inspiration, Optimism, Spirituality, Dreams, Progress to Goals'},
      {id: 17, name: 'stork', front: card_17, back: card_2_back, keywords: 'Change, Transition, Movement, Recurrence, New Cycle, Yearning'},
      {id: 18, name: 'dog', front: card_18, back: card_3_back, keywords: 'Loyalty, Friendship, A Follower, Devotion, Obedience, Support'},
      {id: 19, name: 'tower', front: card_19, back: card_4_back, keywords: 'Authority, Solitude, Loneliness, Isolation, Aloofness, Ego, Arrogance'},
      {id: 20, name: 'garden', front: card_20, back: card_5_back, keywords: 'Public Affairs, Society, Culture, Teamwork, Fame, Social Networks'},
      {id: 21, name: 'mountain', front: card_21, back: card_1_back, keywords: 'Difficulties, Problems, Obstacles, Impairment, Hurdles, Struggles, Challenge'},
      {id: 22, name: 'crossroads', front: card_22, back: card_2_back, keywords: 'Choices, Many Opportunities, Travel, Separation, Hesitation, Decisions'},
      {id: 23, name: 'mice', front: card_23, back: card_3_back, keywords: 'Dwindling, Deficiency, Depletion, Destruction, Defect, Flaw, Disease'},
      {id: 24, name: 'heart', front: card_24, back: card_4_back, keywords: 'Love, Amicability, Romanticization, Forgiveness, Reconciliation, Softness, Charity'},
      {id: 25, name: 'ring', front: card_25, back: card_5_back, keywords: 'Commitment, Promise, Honor, Partnership, Cooperation, Cycles'},
      {id: 26, name: 'book', front: card_26, back: card_1_back, keywords: 'Secrets, Knowledge, Education, Information, Research, Studies'},
      {id: 27, name: 'letter', front: card_27, back: card_2_back, keywords: 'Document, Email, Speech, Conversations, Expression, Information, Communication'},
      {id: 28, name: 'man', front: card_28, back: card_3_back, keywords: 'Male Friend, Partner, Family Member, The Querent, Masculinity'},
      {id: 29, name: 'woman', front: card_29, back: card_4_back, keywords: 'Female Friend, Partner, Family Member, A Female Querent, Femininity'},
      {id: 30, name: 'lily', front: card_30, back: card_5_back, keywords: 'Sensuality, Sex, Virtue, Morality, Ethics, Wisdom'},
      {id: 31, name: 'sun', front: card_31, back: card_1_back, keywords: 'Happiness, Victory, Success, Power, Warmth, Truth'},
      {id: 32, name: 'moon', front: card_32, back: card_2_back, keywords: 'Subconscious, Intuition, Emotions, Fears, Desires, Fantasy'},
      {id: 33, name: 'key', front: card_33, back: card_3_back, keywords: 'Openness, Revelation, Unlocking, Achievement, Liberation, Resolution'},
      {id: 34, name: 'fish', front: card_34, back: card_4_back, keywords: 'Finances, Business, Wealth, Values, Gain, Abundance'},
      {id: 35, name: 'anchor', front: card_35, back: card_5_back, keywords: 'Stability, Restraint, Security, Resilience, Durability, Laying Foundations'},
      {id: 36, name: 'cross', front: card_36, back: card_1_back, keywords: 'Duty, Conviction, Suffering, Burden, Intolerance, Principles, Indoctrination'}
    ];
  }

  shuffle() {
    const { deck } = this;
    let m = deck.length, i;

    while (m) {
      i = Math.floor(Math.random() * m--);

      [deck[m], deck[i]] = [deck[i], deck[m]];
    }

    return this;
  }

}

export default Deck;
